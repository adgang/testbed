# Strategy Testbed

## Prerequisites

1. Eclipse(works on 4.7 Oxygen)
2. Efxplugin
3. Gradle plugin


## Building and Running the application

You can simply import the project into Eclipse as a gradle project, then run and debug it seamlessly. Don't forget to refresh the project once after eclipse builds it, as the fxml file has to be available.

To run the application as a jar, open the build.fxbuild file using FXBuild Configuration Editor(Efx plugin should provide this). On the right section of the file layout, under 'Building and Exporting' you see a link 'Generate ant build.xml and run'; click on it. Now a testbed.jar is created in `build/dist`. I still haven't figured how to make this build a fat jar.


## API Mock

If Bitso kicks you out(quota exhaustion) or the server is down, run the application with `--mock-apis` as argument and it will run and fill some dummy values for the initial order book and trades.

## Logs

The application logs to `logs/application.log`. You can change the log4j2 config in `src/main/resources`


## Code Organization

1. Main class: simply starts a JavaFx application with TestbedMonitorController.
2. UI:
    * The UI has real trades, fake trades, bids and asks.
    * The UI allows to set the number of up ticks that trigger a buy(M) and number of down ticks that trigger a sell(N) for the strategy mentioned in the problem.
    * It also allows you to set how many rows to display on order tables and trade tables.
    * There is also a display for net profit(at current BTC prices) and current position of BTC and MXN.
3. TradeSimulationService: a simple service which accepts fake trades(a real trade service would have accepted market orders) and publishes to whoever wishes to listen(in our case UI)
4. Strategies: MarketMakerStrategy listens to trade updates from market and based on some rules(buy on m upticks, sell on n downticks. m and n can be changed in UI runtime). There is code for one more example strategy called UpUpUpStrategy but it is not wired to UI.

The application subscribes to order updates and calls order book api. Once the order book is ready, the updates are applied over it. Trades are polled every 10s(which seemed more than enough).


## TODOs

Major project level TODOs are
1. Handle an out of sequence trade or order updates(by dropping the whole table and getting it fresh) or basically all error handling wrt server
2. Optimization: Too many double stored lists are present. Replication of code, data is present.
3. Bad UI binding to variables. You can't blame a rookie JavaFX guy much here.

## FAQs

1. Why should I build jar in this crazy manner?

    If you can just use eclipse to run the application without needing a jar, it . I had to create this project using gradle but rely on fxbuild plugin for build. Both JavaFX and gradle are new to me and I would rather present the application working as it is than burn more time getting the build right.
2. Why is there a lot of unused code?
3. Why is the code so crappy?

    See the next section

## Some Words on the Code

I completed the functionality but still have to do a lot of refactoring. Considering the major delay so far, I thought it would be good to just publish whatever I have and let you see the app working. The organization is not so apparent, but the code works correctly with no synchronization issues and no waiting/loops(except the one specifically asked to be done: polling trades). All services and strategies would be event driven, where event is a trade(implemented) or order(not implemented) update.


## Checklist

Feature                                                                 | File name                                                 | Method name
------------------------------------------------------------------------|-----------------------------------------------------------|-------
Schedule the polling of trades over REST.                               | org/sonar/testbed/ui/TestbedMonitorController.java        |   startTradeUpdateThread
Request a book snapshot over REST.                                      | org/sonar/testbed/services/BitsoService.java              |   getOrderBook
Listen for diff-orders over websocket.                                  | org/sonar/testbed/services/BitsoService.java              |   listenToOrderUpdates
Replay diff-orders.                                                     | org/sonar/testbed/OrderTable.java                         |   fetchOrders, onNext, updateOrderBook
Use config option X to request  recent trades.                          | org/sonar/testbed/ui/TestbedMonitorController.java        |   refreshTrades
Use config option X to limit number of ASKs displayed in UI.            | org/sonar/testbed/ui/TestbedMonitorController.java        |   refreshOrders
The loop that causes the trading algorithm to reevaluate.               | No separate loop, evaluation happens on each trade update.  The trigger is strategy.OnTradeUpdate in TestbedMonitorController.java |
