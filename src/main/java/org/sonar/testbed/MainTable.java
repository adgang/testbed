package org.sonar.testbed;

import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.fxml.FXMLLoader;

import org.sonar.testbed.services.BitsoService;
import org.sonar.testbed.types.ObservableArrayList;
import org.sonar.testbed.types.OrderBook;
import org.sonar.testbed.types.OrderBookHandler;
import org.sonar.testbed.types.OrderMap;
import org.sonar.testbed.types.OrderRow;
import org.sonar.testbed.types.OrderUpdateMessage;

import io.reactivex.*;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MainTable extends Application {
    public static Logger log = LogManager.getLogger("org.sonar.testbed");
    private TableView<OrderRow> tableView;
    private OrderTable orderTable;

    public class Person {
        private StringProperty firstName;
        public void setFirstName(String value) { firstNameProperty().set(value); }
        public String getFirstName() { return firstNameProperty().get(); }
        public StringProperty firstNameProperty() {
            if (firstName == null) firstName = new SimpleStringProperty(this, "firstName");
            return firstName;
        }

        private StringProperty lastName;
        public void setLastName(String value) { lastNameProperty().set(value); }
        public String getLastName() { return lastNameProperty().get(); }
        public StringProperty lastNameProperty() {
            if (lastName == null) lastName = new SimpleStringProperty(this, "lastName");
            return lastName;
        }
    }


    @Override
    public void start(Stage primaryStage) {

        Flowable.just("Hello world").subscribe((msg) -> log.info(msg));
//        BitsoService.getBooks();
//        BitsoService.getOrderBook();
//        BitsoService.getTrades();
//        try {
//            BitsoService.listenToOrderUpdates();
//        } catch (URISyntaxException e1) {
//            // TODO Auto-generated catch block
//            e1.printStackTrace();
//        }
        try {


            BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("TestbedMonitor.fxml"));
            Scene scene = new Scene(root, 800, 400);

            TableView<Person> table = new TableView<Person>();


            ObservableList<Person> teamMembers = new SortedList<Person>((new ObservableArrayList<Person>()));
            table.setItems(teamMembers);


            TableColumn<Person,String> firstNameCol = new TableColumn<Person,String>("First Name");
            firstNameCol.setCellValueFactory(new PropertyValueFactory<Person, String>("firstName"));
            TableColumn<Person,String> lastNameCol = new TableColumn<Person,String>("Last Name");
            lastNameCol.setCellValueFactory(new PropertyValueFactory<Person, String>("lastName"));

            table.getColumns().setAll(firstNameCol, lastNameCol);

            StackPane pane = (StackPane)root.getChildren().get(1);
            pane.getChildren().add(table);


            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
