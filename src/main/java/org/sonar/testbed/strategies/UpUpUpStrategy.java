package org.sonar.testbed.strategies;

import java.util.Date;
import java.util.Queue;

import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.apache.logging.log4j.Logger;
import org.sonar.testbed.Utils;
import org.sonar.testbed.services.TradeSimulationService;
import org.sonar.testbed.types.Side;
import org.sonar.testbed.types.Trade;

/**
 * On 3 consecutive upticks, buy a bitcoin equivalent of last trade
 * @author adgang
 *
 */
public class UpUpUpStrategy implements Strategy {

    private static final Logger log = Utils.getLogger(UpUpUpStrategy.class);

    private static final String PATTERN = "[U, U, U]";
    private Queue<Character> history = new CircularFifoQueue<Character>(3);
    private double precision = 0.00;

    private double currentPrice;
    private TradeSimulationService tradeService;
    private long tradeId = 0;

    public UpUpUpStrategy(TradeSimulationService service) {
        this.tradeService = service;
    }

    @Override
    public void OnTradeUpdate(Trade t) {
        if (currentPrice == 0) {
            // first trade update
            currentPrice = t.getPrice();
        } else {
            history.add(determineTick(currentPrice, t));
            log.trace(history);
            if (history.toString().equals(PATTERN)) {
                Trade newDummyTrade = new Trade(++tradeId, new Date(),
                        Side.BUY.toString(), t.getAmount(), t.getPrice());
                tradeService.registerTrade(newDummyTrade);
                log.trace(newDummyTrade);
            }
        }

    }

    public char determineTick(double lastPrice, Trade t) {
        if (Double.compare(lastPrice, t.getPrice()) == 0 || Math.abs(lastPrice - t.getPrice()) <= precision) {
            return 'Z';
        } else if (lastPrice > t.getPrice()) {
            return 'D';
        } else {
            return 'U';
        }
    }

    @Override
    public double getProfit() {
        // TODO Auto-generated method stub
        return 0;
    }
}
