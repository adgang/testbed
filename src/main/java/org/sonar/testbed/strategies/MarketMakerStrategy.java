package org.sonar.testbed.strategies;

import java.text.DecimalFormat;
import java.util.Date;

import org.apache.logging.log4j.Logger;
import org.sonar.testbed.Utils;
import org.sonar.testbed.services.TradeSimulationService;
import org.sonar.testbed.types.Position;
import org.sonar.testbed.types.Side;
import org.sonar.testbed.types.Trade;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MarketMakerStrategy implements Strategy {

    private static final Logger log = Utils.getLogger(MarketMakerStrategy.class);
    private static final DecimalFormat decimalFormat = new DecimalFormat("#0.0############");


    private int uptickCountTrigger = 3;
    private int downtickCountTrigger = 3;

    private double currentPrice;
    private TradeSimulationService tradeService;
    private long tradeId = 0;

    public MarketMakerStrategy(TradeSimulationService service) {
        this.tradeService = service;
        upTickCountProperty().set(uptickCountTrigger);
        downTickCountProperty().set(downtickCountTrigger);
        positionProperty().set(positionString());
    }

    @Override
    public void OnTradeUpdate(Trade t) {
        trackTicks(t);
    }

    private int upTicks, downTicks;
    private int maxInventory = 1;
    private int minInventory = 0;


    public void trackTicks(Trade t) {
        if (currentPrice == 0) {
            // first trade update
            currentPrice = t.getPrice();
            upTicks = 0;
            downTicks = 0;
        } else {
            log.trace(t);
            int tickSide = determineTick(currentPrice, t);
            // uptick
            if (tickSide > 0) {
                downTicks = 0;
                upTicks += 1;
            } else if (tickSide < 0) {
                upTicks = 0;
                downTicks += 1;
            }
            if (upTicks == uptickCountTrigger) {
                buyBitCoin(t.getPrice());
            } else if (downTicks == downtickCountTrigger) {
                sellBitCoin(t.getPrice());
            }
        }

    }

    public void buyBitCoin(double price) {
        transactBitCoin(price, Side.BUY);
    }

    public void sellBitCoin(double price) {
        transactBitCoin(price, Side.SELL);
    }

    public void transactBitCoin(double price, Side side) {
        Position position = tradeService.getPosition();
        if (side == Side.BUY && position.getBtc() < maxInventory
                || side == Side.SELL && position.getBtc() > minInventory) {
            Trade newDummyTrade = new Trade(++tradeId, new Date(),
                    side.toString(), 1, price);
            tradeService.registerTrade(newDummyTrade);
            log.trace(side.toString() + "ing bitcoin at " + price);
        }
        upTicks = downTicks = 0;
        profitProperty().set(decimalFormat.format(getProfit()));
        positionProperty().set(positionString());
    }

    // Kick this into an abstract strategy class
    public int determineTick(double lastPrice, Trade t) {
        int tickSide = Double.compare(t.getPrice(), lastPrice);
        currentPrice = t.getPrice();
        return tickSide;
    }

    public String positionString() {
        return tradeService.getPosition().getBtc()
                + " | " + decimalFormat.format(tradeService.getPosition().getMxn());
    }

    @Override
    public double getProfit() {
        return tradeService.getNetPosition();
    }

    /**
     * @return the uptickCountTrigger
     */
    public int getUptickCountTrigger() {
        return uptickCountTrigger;
    }

    /**
     * @param uptickCountTrigger the uptickCountTrigger to set
     */
    public void setUptickCountTrigger(int uptickCountTrigger) {
        this.uptickCountTrigger = uptickCountTrigger;
        upTickCountProperty().setValue(uptickCountTrigger);
    }


    private IntegerProperty upTickCountProperty;
    public IntegerProperty upTickCountProperty() {
        if (upTickCountProperty == null) upTickCountProperty = new SimpleIntegerProperty(this, "upTickCount");
        return upTickCountProperty;

    }

    /**
     * @return the downtickCountTrigger
     */
    public int getDowntickCountTrigger() {
        return downtickCountTrigger;
    }

    /**
     * @param downtickCountTrigger the downtickCountTrigger to set
     */
    public void setDowntickCountTrigger(int downtickCountTrigger) {
        this.downtickCountTrigger = downtickCountTrigger;
        downTickCountProperty().setValue(downtickCountTrigger);
    }


    private IntegerProperty downTickCountProperty;
    public IntegerProperty downTickCountProperty() {
        if (downTickCountProperty == null) downTickCountProperty = new SimpleIntegerProperty(this, "downTickCount");
        return downTickCountProperty;

    }

    private StringProperty profitProperty;
    public StringProperty profitProperty() {
        if (profitProperty == null) profitProperty = new SimpleStringProperty(this, "profit");
        return profitProperty;
    }

    private StringProperty positionProperty;
    public StringProperty positionProperty() {
        if (positionProperty == null) positionProperty = new SimpleStringProperty(this, "position");
        return positionProperty;
    }

}
