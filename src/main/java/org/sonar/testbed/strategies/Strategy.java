package org.sonar.testbed.strategies;

import org.sonar.testbed.types.Trade;

public interface Strategy {
    // do something based on a trade update
    void OnTradeUpdate(Trade t);

    // get Profit of Loss made by strategy
    double getProfit();

    // TODO: take action on order update to improve strategy perf
    // do something on order update
    // void OnOrderUpdate(OrderUpdate o);
}
