package org.sonar.testbed;

import java.net.URISyntaxException;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.sonar.testbed.services.BitsoService;
import org.sonar.testbed.types.Order;
import org.sonar.testbed.types.OrderBook;
import org.sonar.testbed.types.OrderBookHandler;
import org.sonar.testbed.types.OrderUpdate;
import org.sonar.testbed.types.OrderUpdateMessage;
import org.sonar.testbed.types.Side;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

// TODO: Implement message queue to async the websocket thread

public class OrderTable implements Observer<OrderUpdateMessage>, Disposable {

    /**
     * Generated uid to silence serialization warning
     */
    @SuppressWarnings("unused")
    private static final long serialVersionUID = -6067180044221993300L;
    private static final Logger log = Utils.getLogger(OrderTable.class);
    private Disposable subscription;
    private OrderBookHandler handler;
    private OrderBook orderBook;
    private Observable<OrderUpdateMessage> orderUpdateObservable = null;
    private int orderBookDepth = 15;


    public OrderTable(OrderBookHandler handler) {
        super();
        this.handler = handler;
    }

    /**
     * @return the orderBookDepth
     */
    public int getOrderBookDepth() {
        return orderBookDepth;
    }

    /**
     * @param orderBookDepth the orderBookDepth to set
     */
    public void setOrderBookDepth(int orderBookDepth) {
        this.orderBookDepth = orderBookDepth;
    }

    public OrderBook getOrderBook() {
        return orderBook;
    }

    // reuses an existing observable
    public Observable<OrderUpdateMessage> fetchOrders(Observable<OrderUpdateMessage> obs) throws URISyntaxException {
        if (orderUpdateObservable == null) {
            synchronized (this) {
                if (orderUpdateObservable == null) {
                    orderUpdateObservable = obs;
                    orderUpdateObservable.subscribe(this);
                }
            }
        }
        return orderUpdateObservable;
    }

    public Observable<OrderUpdateMessage> fetchOrders() throws URISyntaxException {
        if (orderUpdateObservable == null) {
            synchronized (this) {
                if (orderUpdateObservable == null) {
                    orderUpdateObservable = BitsoService.listenToOrderUpdates();
                    orderUpdateObservable.subscribe(this);
                }
            }
        }
        return orderUpdateObservable;
    }

    // TODO: Update time and sequence and check monotonicity of sequence
    public void updateOrderBook(OrderUpdateMessage update) {
        if (update.getSequence() > orderBook.getSequence()) {
            update.getOrderUpdates().forEach(u -> updateWithOrder(u));
        }
    }

    public List<? extends Order> getBids() {
        return getBids(orderBookDepth);
    }

    public List<? extends Order> getBids(int n) {
        return orderBook.getBids().top(n);
    }

    public List<? extends Order> getAsks() {
        return getAsks(orderBookDepth);
    }

    public List<? extends Order> getAsks(int n) {
        return orderBook.getAsks().top(n);
    }

    public void updateWithOrder(OrderUpdate update) {
        if (update.getSide().equals(Side.BUY)) {
            orderBook.getBids().updateWithOrder(update);
        } else if (update.getSide().equals(Side.SELL)) {
            orderBook.getAsks().updateWithOrder(update);
        } else {
            log.error("Unknown side:" + update.getSide());
            return;
        }
    }

    public boolean isReady() {
        return orderBook != null;
    }

    /**
     * Implementing Observer
     */
    @Override
    public void onSubscribe(Disposable d) {
        subscription = d;
    }

    @Override
    public void onNext(OrderUpdateMessage m) {
        if (!isReady()) {
            orderBook = BitsoService.getOrderBook();
            this.handler.onOrderBookReady(orderBook);
        }
        updateOrderBook(m);
    }

    @Override
    public void onError(Throwable e) {
        handler.handleError(e);
    }

    @Override
    public void onComplete() {
        log.debug("completing this order table");
        handler.handleError(new Exception("Unexpected completion of subject"));
    }

    /**
     * Implements Disposable
     */
    private boolean disposed = false;

    @Override
    public void dispose() {
        if (!disposed) {
            synchronized (this) {
                if (!disposed) {
                    subscription.dispose();
                    disposed = true;
                }
            }
        }
    }

    @Override
    public boolean isDisposed() {
        return disposed;
    }

}
