package org.sonar.testbed.ui;

import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;

import org.apache.logging.log4j.Logger;
import org.sonar.testbed.OrderTable;
import org.sonar.testbed.Utils;
import org.sonar.testbed.services.BitsoService;
import org.sonar.testbed.services.TradeSimulationService;
import org.sonar.testbed.strategies.MarketMakerStrategy;
import org.sonar.testbed.strategies.Strategy;
import org.sonar.testbed.types.Order;
import org.sonar.testbed.types.OrderBook;
import org.sonar.testbed.types.OrderBookHandler;
import org.sonar.testbed.types.OrderBookUpdate;
import org.sonar.testbed.types.OrderMap;
import org.sonar.testbed.types.OrderRow;
import org.sonar.testbed.types.OrderUpdateMessage;
import org.sonar.testbed.types.Trade;
import org.sonar.testbed.types.TradeRow;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.util.converter.NumberStringConverter;
import javafx.scene.control.Button;

public class TestbedMonitorController implements OrderBookHandler, Initializable {

    private static final int TRADES_POLLING_TIMEOUT = 10000;
    private static final int DEFAULT_ORDERBOOK_DEPTH = 15;
    private static final int DEFAULT_TRADEBOOK_DEPTH = 15;
    private static final Logger log = Utils.getLogger(TestbedMonitorController.class);

    @FXML
    TableView<OrderRow> bidsView = new TableView<OrderRow>();

    @FXML
    TableView<OrderRow> asksView = new TableView<OrderRow>();

    @FXML
    TableView<TradeRow> realTradesView = new TableView<TradeRow>();

    ObservableList<Trade> realTradeList = FXCollections.observableArrayList(new ArrayList<Trade>());
    ObservableList<Trade> simulatedTradeList = FXCollections.observableArrayList(new ArrayList<Trade>());

    ObservableList<OrderRow> bidsList = FXCollections.observableArrayList(new ArrayList<OrderRow>());
    ObservableList<OrderRow> asksList = FXCollections.observableArrayList(new ArrayList<OrderRow>());

    ObservableList<Trade> fullRealTradeList = FXCollections.observableArrayList(new ArrayList<Trade>());
    ObservableList<Trade> fullSimulatedTradeList = FXCollections.observableArrayList(new ArrayList<Trade>());

    @FXML
    TableView<TradeRow> simulatedTradesView = new TableView<TradeRow>();

    @FXML
    TextField upTickCount;

    @FXML
    TextField downTickCount;

    @FXML
    TextField orderBookDepth;

    @FXML
    TextField tradeBookDepth;

    private OrderTable orderTable;
    private Observable<OrderUpdateMessage> orderObservable;

    public TestbedMonitorController() {
        try {
            bidsView.setItems(bidsList);
            asksView.setItems(asksList);

            Observable<OrderUpdateMessage> obs = BitsoService.listenToOrderUpdates();
            this.orderObservable = obs;
            orderTable = new OrderTable(this);
            orderTable.fetchOrders(obs);
        } catch(Exception e) {
            log.error(e);
            // TODO: handle this or kick to init
        }
    }

    @Override
    public void handleError(Throwable t) {
        // TODO Auto-generated method stub

    }

    // get top elements from a source list
    public <T> void refreshTopList(List<T> topList, List<T> list, int topCount) {
        for (int i = 0; i < topCount; i++) {
            if (i >= list.size()) {
                break;
            }
            T candidate = list.get(i);
            if (i == topList.size()) {
                topList.add(candidate);
            } else {
                topList.set(i, candidate);
            }
        }
        // remove excess elements
        for (int i = topList.size() - 1; i >= topCount; i--) {
            topList.remove(i);
        }
    }

    public int getOrderBookDepth() {
        int depth = DEFAULT_ORDERBOOK_DEPTH;
        try {
            depth = Integer.parseInt(orderBookDepth.getText());
        } catch (NumberFormatException e) {
        }
        return depth;
    }

    public int getTradeBookDepth() {
        int depth = DEFAULT_TRADEBOOK_DEPTH;
        try {
            depth = Integer.parseInt(tradeBookDepth.getText());
        } catch (NumberFormatException e) {
        }
        return depth;
    }

    public void refreshOrders() {
        refreshTopList(bidsList, (List) orderTable.getOrderBook().getBids().getOrders(), getOrderBookDepth());
        refreshTopList(asksList, (List) orderTable.getOrderBook().getAsks().getOrders(), getOrderBookDepth());
    }

    public void refreshTrades() {
        Comparator<Trade> newestGreatComparator = new Comparator<Trade>() {

            @Override
            public int compare(Trade o1, Trade o2) {
                // reverse the order so that newest comes on top
                return Long.compare(o2.getId(), o1.getId());
            }
        };
        fullRealTradeList.sort(newestGreatComparator);
        refreshTopList(realTradeList, fullRealTradeList, getTradeBookDepth());
        fullSimulatedTradeList.sort(newestGreatComparator);
        refreshTopList(simulatedTradeList, fullSimulatedTradeList, getTradeBookDepth());
    }

    @Override
    public void onOrderBookReady(OrderBook book) {
        bidsView.setItems(bidsList);
        asksView.setItems(asksList);

        refreshOrders();
        orderObservable.subscribe(new Observer<OrderUpdateMessage>() {

            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(OrderUpdateMessage t) {
                refreshOrders();
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onComplete() {
                // TODO Auto-generated method stub

            }
        });
    }

    private void addOrderCols(TableView<OrderRow> tableView) {
        TableColumn<OrderRow, String> orderIdCol = new TableColumn<OrderRow, String>("Order Id");
        orderIdCol.setCellValueFactory(new PropertyValueFactory<OrderRow, String>("orderId"));

        TableColumn<OrderRow, String> priceCol = new TableColumn<OrderRow, String>("Price");
        priceCol.setCellValueFactory(new PropertyValueFactory<OrderRow, String>("price"));

        TableColumn<OrderRow, String> amountCol = new TableColumn<OrderRow, String>("Amount");
        amountCol.setCellValueFactory(new PropertyValueFactory<OrderRow, String>("amount"));
        tableView.getColumns().setAll(orderIdCol, priceCol, amountCol);
        tableView.getColumns().forEach(c -> c.prefWidthProperty().bind(tableView.widthProperty().divide(3)));
    }

    private void addTradeCols(TableView<TradeRow> tableView) {
        TableColumn<TradeRow, String> tradeIdCol = new TableColumn<TradeRow, String>("Trade Id");
        tradeIdCol.setCellValueFactory(new PropertyValueFactory<TradeRow, String>("id"));

        TableColumn<TradeRow, String> createdCol = new TableColumn<TradeRow, String>("Created");
        createdCol.setCellValueFactory(new PropertyValueFactory<TradeRow, String>("creationTime"));

        TableColumn<TradeRow, String> sideCol = new TableColumn<TradeRow, String>("Side");
        sideCol.setCellValueFactory(new PropertyValueFactory<TradeRow, String>("side"));

        TableColumn<TradeRow, String> priceCol = new TableColumn<TradeRow, String>("Price");
        priceCol.setCellValueFactory(new PropertyValueFactory<TradeRow, String>("price"));

        TableColumn<TradeRow, String> amountCol = new TableColumn<TradeRow, String>("Amount");
        amountCol.setCellValueFactory(new PropertyValueFactory<TradeRow, String>("amount"));

        tableView.getColumns().setAll(tradeIdCol, createdCol, sideCol, priceCol, amountCol);
        tableView.getColumns().forEach(c -> c.prefWidthProperty().bind(tableView.widthProperty().divide(5)));

    }

    // TODO: Polling for trades is a bad strategy anyway irrespective of the timeout.
    // Listen the same way as orders are listened to
    public void updateTrades() {
        while (true) {
            try {
                int pollNumber = queue.take();
                log.info("polling for trade for times:" + pollNumber);
                List<Trade> tradesBatch = BitsoService.getTrades();
                tradesBatch.sort(new Comparator<Trade>() {

                    @Override
                    public int compare(Trade o1, Trade o2) {
                        // reverse the order so that newest comes on top
                        return Long.compare(o1.getId(), o2.getId());
                    }
                });
                tradesBatch.forEach(t -> {
                    // Add if the trade is already not recorded
                    // TODO: Map would be more efficient. TradeBook like OrderBook
                    if (!this.fullRealTradeList.stream().anyMatch(c -> c.getId() == t.getId())) {
                        this.fullRealTradeList.add(t);
                        strategy.OnTradeUpdate(t);
                    }
                });
            } catch (InterruptedException e) {
                log.info("stopping polling for trades");
                break;
            }
            refreshTrades();
        }
    }

    public void startTradeUpdateThread() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                updateTrades();
            }
        }).start();
    }

    private ArrayBlockingQueue<Integer> queue = new ArrayBlockingQueue<Integer>(3);
    private MarketMakerStrategy strategy;
    @FXML Button setTicks;
    @FXML Button setOrdersDepth;
    @FXML Button setTradesDepth;

    @FXML TextField profit;
    @FXML TextField position;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        addOrderCols(bidsView);
        addOrderCols(asksView);
        addTradeCols(realTradesView);
        addTradeCols(simulatedTradesView);
        realTradesView.setItems((ObservableList)realTradeList);
        simulatedTradesView.setItems((ObservableList)simulatedTradeList);
        TradeSimulationService simulator = new TradeSimulationService();
        strategy = new MarketMakerStrategy(simulator);
        setTicks.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                if (event.getEventType().equals(MouseEvent.MOUSE_CLICKED)) {
                    strategy.setUptickCountTrigger(Integer.parseInt(upTickCount.getText()));
                    strategy.setDowntickCountTrigger(Integer.parseInt(downTickCount.getText()));
                }
            }
        });

        setOrdersDepth.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getEventType().equals(MouseEvent.MOUSE_CLICKED)) {
                    refreshOrders();
                }
            }
        });

        setTradesDepth.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getEventType().equals(MouseEvent.MOUSE_CLICKED)) {
                    refreshTrades();
                }
            }
        });

        // Using Strategy interface to avoid writing another full hoopla class
        // Listens to simulator for imaginary trades(to update the table)
        simulator.addListener(new Strategy() {

            @Override
            public double getProfit() {
                return simulator.getNetPosition();
            }

            @Override
            public void OnTradeUpdate(Trade t) {
                fullSimulatedTradeList.add(t);
                log.trace("profit at current BTC prices:" + simulator.getNetPosition());
            }
        });

        upTickCount.textProperty().bindBidirectional(strategy.upTickCountProperty(), new NumberStringConverter());
        downTickCount.textProperty().bindBidirectional(strategy.downTickCountProperty(), new NumberStringConverter());
        profit.textProperty().bind(strategy.profitProperty());
        position.textProperty().bind(strategy.positionProperty());

        startPollingTrades();
        startTradeUpdateThread();
    }

    public void startPollingTrades() {
        // TODO: cleanup thread and write dispose()
        new Thread(new Runnable() {

            @Override
            public void run() {
                int counter = 1;
                while(true) {
                    try {
                        queue.put(counter);
                        Thread.sleep(TRADES_POLLING_TIMEOUT);
                    } catch (InterruptedException e) {
                        log.info("stopping polling for trades");
                        break;
                    }
                    counter++;
                }
            }

        }).start();
    }

}
