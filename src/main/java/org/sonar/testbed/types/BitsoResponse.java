package org.sonar.testbed.types;

import com.fasterxml.jackson.annotation.JsonCreator;

public class BitsoResponse<T> extends JSONStringableObject {

    private boolean success;
    private T payload;
    private BitsoError error;

    /**
     * @param success
     * @param payload
     * @param error
     */
    @JsonCreator
    public BitsoResponse(boolean success, T payload, BitsoError error) {
        this.success = success;
        this.payload = payload;
        this.error = error;
    }

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @return the payload
     */
    protected T getPayload() {
        return payload;
    }

    /**
     * @return the error
     */
    public BitsoError getError() {
        return error;
    }

}
