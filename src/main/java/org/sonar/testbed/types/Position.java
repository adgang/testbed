package org.sonar.testbed.types;

public class Position {

    private double mxn, btc;

    public Position(double mxn, double btc) {
        this.mxn = mxn;
        this.btc = btc;
    }

    public void updateWithTrade(Trade t) {
        if (t.getSide() == Side.SELL) {
            this.btc -= t.getAmount();
            this.mxn += t.getAmount() * t.getPrice();
        } else {
            this.btc += t.getAmount();
            this.mxn -= t.getAmount() * t.getPrice();
        }
    }

    /**
     * @return the mxn
     */
    public double getMxn() {
        return mxn;
    }

    /**
     * @return the btc
     */
    public double getBtc() {
        return btc;
    }

}
