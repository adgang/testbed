package org.sonar.testbed.types;

import java.text.DecimalFormat;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class OrderRow extends Order {

    private StringProperty orderIdProperty;
    private StringProperty priceProperty;
    private StringProperty amountProperty;

    private DecimalFormat df = new DecimalFormat("#0.0############");

    @JsonCreator
    public OrderRow(@JsonProperty("orderId") String orderId, @JsonProperty("price") double price,
            @JsonProperty("amount") double amount) {
        super(orderId, price, amount);
        orderIdProperty().set(orderId);
        priceProperty().set(df.format(price));
        amountProperty().set(df.format(amount));
    }

    public StringProperty orderIdProperty() {
        if (orderIdProperty == null) orderIdProperty = new SimpleStringProperty(this, "orderId");
        return orderIdProperty;
    }

    public StringProperty amountProperty() {
        if (amountProperty == null) amountProperty = new SimpleStringProperty(this, "amount");
        return amountProperty;
    }

    public StringProperty priceProperty() {
        if (priceProperty == null) priceProperty = new SimpleStringProperty(this, "price");
        return priceProperty;
    }

    /* (non-Javadoc)
     * @see org.sonar.testbed.types.Order#setPrice(double)
     */


}
