/**
 *
 */
package org.sonar.testbed.types;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author adgang TODO: Make this immutable
 */
public class OrderUpdate extends JSONStringableObject {

    private String book = "btc_mxn";

    private Date timestamp;
    private double price;
    private Side side;
    private double amount;
    private double value;
    private String orderId;
    private String status;

    /**
     * @param book
     * @param timestamp
     * @param price
     * @param side
     * @param amount
     * @param value
     * @param orderId
     */
    @JsonCreator
    public OrderUpdate(@JsonProperty("d") Date timestamp, @JsonProperty("r") double price, @JsonProperty("t") Side side,
            @JsonProperty("a") double amount, @JsonProperty("v") double value, @JsonProperty("o") String orderId,
            @JsonProperty("s") String status) {
        this.timestamp = timestamp;
        this.price = price;
        this.side = side;
        this.amount = amount;
        this.value = value;
        this.orderId = orderId;
        this.status = status;
    }

    /**
     * @return the book
     */
    public String getBook() {
        return book;
    }

    /**
     * @return the timestamp
     */
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @return the side
     */
    public Side getSide() {
        return side;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @return the value
     */
    public double getValue() {
        return value;
    }

    /**
     * @return the orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

}
