package org.sonar.testbed.types;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

import org.apache.logging.log4j.Logger;
import org.sonar.testbed.Utils;

public class AskTreeMap extends TreeMap<String, Order> {

    /**
     *
     */
    private static final long serialVersionUID = 8399784956173455993L;
    private static final Logger log = Utils.getLogger(AskTreeMap.class);

    public AskTreeMap() {
        // TODO Auto-generated constructor stub
    }

    /* Comparator for Asks
     * @see java.util.TreeMap#comparator()
     */
    @Override
    public Comparator<? super String> comparator() {
        Map<String, Order> map = this;
        return new Comparator<String> () {

            @Override
            public int compare(String o1, String o2) {
                return Double.compare(map.get(o1).getPrice(), map.get(o2).getPrice());
            }

        };
    }

}
