package org.sonar.testbed.types;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class TradeRow extends Trade {

    private StringProperty idProperty;
    private StringProperty creationTimeProperty;
    private StringProperty sideProperty;
    private StringProperty amountProperty;
    private StringProperty priceProperty;

    private DecimalFormat decimalFormat = new DecimalFormat("#0.0############");
    private DateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");


    @JsonCreator
    public TradeRow(@JsonProperty("tid") long id, @JsonProperty("created_at") Date creationTime,
            @JsonProperty("maker_side") String side, @JsonProperty("amount") double amount,
            @JsonProperty("price") double price) {
        super(id, creationTime, side, amount, price);
        idProperty().set(Long.toString(id));
        creationTimeProperty().set(timeFormat.format(creationTime));
        sideProperty().set(side);
        priceProperty().set(decimalFormat.format(price));
        amountProperty().set(decimalFormat.format(amount));

    }

    public StringProperty idProperty() {
        if (idProperty == null) idProperty = new SimpleStringProperty(this, "tradeId");
        return idProperty;
    }

    public StringProperty sideProperty() {
        if (sideProperty == null) sideProperty = new SimpleStringProperty(this, "side");
        return sideProperty;
    }

    public StringProperty creationTimeProperty() {
        if (creationTimeProperty == null) creationTimeProperty = new SimpleStringProperty(this, "created");
        return creationTimeProperty;
    }

    public StringProperty amountProperty() {
        if (amountProperty == null) amountProperty = new SimpleStringProperty(this, "amount");
        return amountProperty;
    }

    public StringProperty priceProperty() {
        if (priceProperty == null) priceProperty = new SimpleStringProperty(this, "price");
        return priceProperty;
    }
}
