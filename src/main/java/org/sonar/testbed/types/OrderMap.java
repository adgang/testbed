package org.sonar.testbed.types;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;
import org.sonar.testbed.Utils;

import javafx.collections.FXCollections;

public class OrderMap extends ObservableArrayList<OrderRow>{

    private static final long serialVersionUID = -7925055345579797149L;
    private static final Logger log = Utils.getLogger(OrderMap.class);
    private List<OrderRow> list = FXCollections.observableArrayList(new ArrayList<OrderRow>());
    private Map<String, Order> map = new HashMap<String, Order>();
    private Object lock = new Object();
    private Comparator<Order> comparator;
    private List<ChangeListener> listeners = new ArrayList<>();

    public OrderMap() {
        this(new Comparator<Order>() {
            @Override
            public int compare(Order o1, Order o2) {
                return Double.compare(o2.getPrice(), o1.getPrice());
            }
        });
    }

    public OrderMap(Comparator<Order> comparator) {
        this.comparator = comparator;
    }

    /**
     *  Implementaton of  List<Order>
     */

    public int size() {
        synchronized (lock) {
            return list.size();
        }
    }

    public Order get(String id) {
        return map.get(id);
    }

    public OrderRow get(int index) {
        return list.get(index);
    }

    public boolean isEmpty() {
        synchronized (lock) {
            return list.isEmpty();
        }
    }

    public boolean contains(Object o) {
        synchronized (lock) {
            return map.containsKey(((Order) o).getOrderId());
        }
    }

    public List<Order> getOrders() {
        return (List)list;
    }

    public List<Order> top(int n) {
        int limit = list.size() > n ? n : list.size();
        return (List)list.subList(0, limit);
    }

    public Iterator<OrderRow> iterator() {
        synchronized (lock) {
            return ((List)list).iterator();
        }
    }

    public Object[] toArray() {
        synchronized (lock) {
            return list.toArray();
        }
    }

    public <T> T[] toArray(T[] a) {
        synchronized (lock) {
            return list.toArray(a);
        }
    }

    public int add(Order o) {
        synchronized (lock) {
            return addUnprotected(o);
        }
    }

    public List<String> getOrderIds() {
        return list.stream().map(o -> o.getOrderId()).collect(Collectors.toList());
    }

    public class Change {
        private int from, to;
        private String id;

        Change(String id, int from, int to) {
            this.id = id;
            this.from = from;
            this.to = to;
        }

        /**
         * @return the from
         */
        public String getId() {
            return id;
        }

        /**
         * @return the from
         */
        public int getFrom() {
            return from;
        }

        /**
         * @return the to
         */
        public int getTo() {
            return to;
        }

    }

    public interface ChangeListener {
        void handleChange(Change c);
    }

    public void addListener(ChangeListener listener) {
        synchronized (lock) {
            listeners.add(listener);
        }
    }

    public void removeListener(ChangeListener listener) {
        synchronized (lock) {
            listeners.remove(listener);
        }
    }

    private void callListeners(Change c) {
        for (ChangeListener l : this.listeners) {
            l.handleChange(c);
        }
    }

    private int addUnprotected(Order o) {
        return addUnprotected(o, true);
    }

    private int addUnprotected(Order o, boolean triggerListener) {
        if (map.get(o.getOrderId()) == null) {
            int indexFound = Collections.binarySearch(list, o, comparator);
            int index = indexFound >= 0 ? indexFound : - indexFound - 1;
            map.put(o.getOrderId(), o);
            OrderRow row = new OrderRow(o.getOrderId(), o.getPrice(), o.getAmount());
            list.add(index, row);
            if (triggerListener) {
                callListeners(new Change(o.getOrderId(), -1, index));
            }
            return index;
        } else {
            log.trace(map);
            log.trace(list);
            log.trace(o);
            if (list.size() > 0) {
                log.trace(o.equals(list.get(0)));
            }

            int currentIndex = -1;
            for (Order or: list) {
                currentIndex++;
                if (or.getOrderId() == o.getOrderId())
                    break;
            }
            log.trace(currentIndex);
            if (o.isIdentical(list.get(currentIndex))) {
                return currentIndex;
            }
            int indexFound = removeUnprotected(o.getOrderId());
            int index = addUnprotected(o, false);
            if (triggerListener) {
                callListeners(new Change(o.getOrderId(), currentIndex, index));
            }
            return index;
        }
    }

    public int remove(String orderId) {
        synchronized (lock) {
            int index = removeUnprotected(orderId);
            if (index >= 0) {
                callListeners(new Change(orderId, index, -1));
            }
            return index;
        }
    }

    private int indexOf(Order o) {
        return list.indexOf(o);
    }

    private int removeUnprotected(String orderId) {
        if (!map.containsKey(orderId)) {
            return -1;
        }
        Order order = map.get(orderId);
        int index = indexOf(order);
        if (index == -1) {
            log.error("List and Map are not in Sync");
        } else {
            log.trace(map);
            log.trace(list);
            list.remove(order);
            map.remove(orderId);
            log.trace(map);
            log.trace(list);

        }
        return index;
    }

    public void updateWithOrder(OrderUpdate update) {
        Order order;
        switch (update.getStatus()) {
        case "open":
            order = new Order(update.getOrderId(), update.getPrice(), update.getAmount());
            this.add(order);
            break;
        case "cancelled":
        case "completed":
            this.remove(update.getOrderId());
            break;
        default:
            log.error("Unknown status of order:" + update);
            break;
        }

    }

}
