package org.sonar.testbed.types;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.sonar.testbed.Utils;

import javafx.beans.InvalidationListener;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

public class ObservableArrayList<T> extends ArrayList<T> implements ObservableList<T> {

    /**
     *
     */
    private static final long serialVersionUID = -5534271795594065437L;
    private static final Logger log = Utils.getLogger(ObservableArrayList.class);
    private List<InvalidationListener> invalidationListerners = new ArrayList<InvalidationListener>();
    private List<ListChangeListener<? super T>> changeListeners = new ArrayList<>();

    @Override
    public void addListener(InvalidationListener listener) {
        invalidationListerners.add(listener);
    }

    @Override
    public void removeListener(InvalidationListener listener) {
        invalidationListerners.remove(listener);
    }

    @Override
    public void addListener(ListChangeListener<? super T> listener) {
        changeListeners.add(listener);
    }

    @Override
    public void removeListener(ListChangeListener<? super T> listener) {
        changeListeners.remove(listener);
    }

    @Override
    public boolean addAll(T... elements) {
        boolean changed = false;
        for (T e : elements) {
            changed = changed || add(e);
        }
        return changed;
    }

    @Override
    public boolean setAll(T... elements) {
        boolean changed = false;
        for (T e : elements) {
            changed = changed || add(e);
        }
        return changed;
    }

    @Override
    public boolean setAll(Collection<? extends T> col) {
        boolean changed = false;
        for (T e : col) {
            changed = changed || add(e);
        }
        return changed;
    }

    @Override
    public boolean removeAll(T... elements) {
        return this.removeAll(Arrays.asList(elements));
    }

    @Override
    public boolean retainAll(T... elements) {
        return this.retainAll(Arrays.asList(elements));
    }

    @Override
    public void remove(int from, int to) {
        for (int j = from; j < to; j++) {
            this.remove(j);
        }
    }

}
