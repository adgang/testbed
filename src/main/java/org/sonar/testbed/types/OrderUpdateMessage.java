package org.sonar.testbed.types;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderUpdateMessage extends JSONStringableObject {

    private String book = "btc_mxn";

    private List<OrderUpdate> orderUpdates;
    private long sequence;

    /**
     * @param orderUpdates
     * @param sequence
     */
    @JsonCreator
    public OrderUpdateMessage(@JsonProperty("payload") List<OrderUpdate> orderUpdates,
            @JsonProperty("sequence") long sequence) {
        this.orderUpdates = orderUpdates;
        this.sequence = sequence;
    }

    /**
     * @return the book
     */
    public String getBook() {
        return book;
    }

    /**
     * @return the orderUpdates
     */
    public List<OrderUpdate> getOrderUpdates() {
        return orderUpdates;
    }

    /**
     * @return the sequence
     */
    public long getSequence() {
        return sequence;
    }

}
