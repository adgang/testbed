package org.sonar.testbed.types;

import java.util.Comparator;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.logging.log4j.Logger;
import org.sonar.testbed.Utils;

public class BidTreeMap extends TreeMap<String, Order> {

    private static final Logger log = Utils.getLogger(BidTreeMap.class);


    public BidTreeMap() {
        super();
    }

    public BidTreeMap(SortedMap<String, Order> map) {
        super(map);
    }

    public Comparator<? super String> getComparator(Map<String, Order> map) {
        Comparator<? super String> comp = new Comparator<String> () {

            @Override
            public int compare(String o1, String o2) {
                return Double.compare(map.get(o2).getPrice(), map.get(o1).getPrice());
            }

        };
        return comp;

    }


    /* Comparator for Asks
     * @see java.util.TreeMap#comparator()
     */
    @Override
    public Comparator<? super String> comparator() {
        BidTreeMap map = this;
        Comparator<? super String> comp = new Comparator<String> () {

            @Override
            public int compare(String o1, String o2) {
                return Double.compare(map.get(o2).getPrice(), map.get(o1).getPrice());
            }

        };
        return comp;

    }
}
