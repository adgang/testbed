package org.sonar.testbed.types;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BitsoError extends JSONStringableObject {

    private String message;
    private String code;

    @JsonCreator
    public BitsoError(@JsonProperty("code") String code, @JsonProperty("message") String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }
}
