package org.sonar.testbed.types;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderBookUpdate extends BitsoResponse<OrderBook> {

    @JsonCreator
    public OrderBookUpdate(@JsonProperty("success") boolean success, @JsonProperty("payload") OrderBook payload,
            @JsonProperty("BitsoError") BitsoError error) {
        super(success, payload, error);
    }

    /**
     * @return the orderBook
     */
    public OrderBook getOrderBook() {

        return getPayload();
    }

}
