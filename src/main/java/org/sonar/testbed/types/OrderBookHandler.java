package org.sonar.testbed.types;

public interface OrderBookHandler extends ErrorHandler {
    void onOrderBookReady(OrderBook book);
}
