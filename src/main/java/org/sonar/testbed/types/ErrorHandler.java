package org.sonar.testbed.types;

public interface ErrorHandler {

    /**
     * Handle the given error, possibly rethrowing it as a fatal exception.
     * copied from springframework.util.ErrorHandler
     */
    void handleError(Throwable t);
}
