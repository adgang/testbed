/**
 *
 */
package org.sonar.testbed.types;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author adgang
 *
 */
public class OrderBook extends JSONStringableObject {
    private Date lastUpdated;
    private OrderMap bids;
    private OrderMap asks;
    private long sequence;

    /**
     * @param lastUpdated
     * @param bids
     * @param asks
     * @param sequence
     */
    @JsonCreator
    public OrderBook(@JsonProperty("updated_at") Date lastUpdated, @JsonProperty("bids") List<Order> bids,
            @JsonProperty("asks") List<Order> asks, @JsonProperty("sequence") long sequence) {
        this.lastUpdated = lastUpdated;
        this.bids = new OrderMap();
        this.asks = new OrderMap(new Comparator<Order>() {

            @Override
            public int compare(Order o1, Order o2) {
                return Double.compare(o1.getPrice(), o2.getPrice());
            }

        });
        bids.forEach(b -> this.bids.add(b));
        asks.forEach(a -> this.asks.add(a));
        this.sequence = sequence;
    }

    /**
     * @return the date
     */
    public Date getLastUpdated() {
        return lastUpdated;
    }

    /**
     * @param date
     *            the date to set
     */
    public void setLastUpdated(Date date) {
        this.lastUpdated = date;
    }

    /**
     * @return the bids
     */
    public OrderMap getBids() {
        return bids;
    }

    /**
     * @return the asks
     */
    public OrderMap getAsks() {
        return asks;
    }

    /**
     * @return the sequence
     */
    public long getSequence() {
        return sequence;
    }

    /**
     * @param sequence
     *            the sequence to set
     */
    public void setSequence(long sequence) {
        this.sequence = sequence;
    }
}
