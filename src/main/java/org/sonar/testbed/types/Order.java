/**
 *
 */
package org.sonar.testbed.types;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author adgang
 *
 */
public class Order extends JSONStringableObject {
    private String book = "btc_mxn";
    private String orderId;
    private double price;
    private double amount;

    @JsonCreator
    public Order(@JsonProperty("oid") String orderId, @JsonProperty("price") double price,
            @JsonProperty("amount") double amount) {
        super();
        this.orderId = orderId;
        this.price = price;
        this.amount = amount;
    }

    public String getBook() {
        return book;
    }

    /**
     * @return the orderId
     */
    public String getOrderId() {
        return orderId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        Order o = (Order) obj;
        return o != null && o.orderId == this.orderId;
    }

    public boolean isIdentical(Object obj) {
        Order o = (Order) obj;
        return o != null && o.orderId == this.orderId && o.book == this.book && o.amount == this.amount
                && o.getPrice() == this.getPrice();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        // TODO Auto-generated method stub
        return orderId.hashCode();
    }
}
