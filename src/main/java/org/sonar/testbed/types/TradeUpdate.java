/**
 *
 */
package org.sonar.testbed.types;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author adgang
 *
 */
public class TradeUpdate extends BitsoResponse<List<TradeRow>> {

    @JsonCreator
    public TradeUpdate(@JsonProperty("success") boolean success, @JsonProperty("payload") List<TradeRow> payload,
            @JsonProperty("error") BitsoError error) {
        super(success, payload, error);
    }

    /**
     * @return the trades
     */
    public List<TradeRow> getTrades() {
        return getPayload();
    }

}
