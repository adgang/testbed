package org.sonar.testbed.types;

public enum Side {
    BUY, SELL
}
