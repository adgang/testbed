package org.sonar.testbed.types;

import org.apache.logging.log4j.Logger;
import org.sonar.testbed.Utils;

import com.fasterxml.jackson.core.JsonProcessingException;

public class JSONStringableObject extends Object {

    private static final Logger log = Utils.getLogger(JSONStringableObject.class);

    @Override
    public String toString() {
        try {
            return Utils.getObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return super.toString();
        }
    }
}
