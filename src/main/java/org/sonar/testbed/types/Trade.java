package org.sonar.testbed.types;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Trade extends JSONStringableObject {

    private String book = "btc_mxn";

    private long id;
    private Date creationTime;
    private Side side;
    private double amount;
    private double price;

    /**
     * @param id
     * @param creationTime
     * @param side
     * @param amount
     * @param price
     */
    @JsonCreator
    public Trade(@JsonProperty("tid") long id, @JsonProperty("created_at") Date creationTime,
            @JsonProperty("maker_side") String side, @JsonProperty("amount") double amount,
            @JsonProperty("price") double price) {
        this.id = id;
        this.creationTime = creationTime;
        this.side = side.toLowerCase().equals("buy") ? Side.BUY : Side.SELL;
        this.amount = amount;
        this.price = price;
    }

    /**
     * @return the book
     */
    public String getBook() {
        return book;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @return the creationTime
     */
    public Date getCreationTime() {
        return creationTime;
    }

    /**
     * @return the side
     */
    public Side getSide() {
        return side;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

}
