package org.sonar.testbed;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;

import org.sonar.testbed.services.BitsoService;

import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main extends Application {
    public static Logger log = LogManager.getLogger(Main.class);

    @Override
    public void start(Stage primaryStage) {
        List<String> args = getParameters().getUnnamed();
        log.info("Stating app with args:" + args);
        if (args.size() >= 1 && args.contains("--mock-apis")) {
            BitsoService.setMockery(true);
            log.warn("Using mock trade and order books instead of calling API");
        }
        try {
            primaryStage.setOnCloseRequest(e -> {
                Platform.exit();
                System.exit(0);
            });

            BorderPane root;
            try {
                root = (BorderPane) FXMLLoader.load(getClass().getResource("TestbedMonitor.fxml"));
                Scene scene = new Scene(root, 800, 400);
                scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
                primaryStage.setTitle("Testbed Monitor");
                primaryStage.setScene(scene);
                primaryStage.show();
            } catch (IOException e) {
                log.error(e);
                e.printStackTrace();
            }

        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
