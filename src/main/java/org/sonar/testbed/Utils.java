package org.sonar.testbed;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

public final class Utils {

    public static final String ISO_8601_PATTERN = "yyyy-MM-dd'T'HH:mm:ssX";

    public static Logger getLogger(final String name) {
        return LogManager.getLogger(name);
    }

    public static Logger getLogger(final Class<?> name) {
        Configurator.setLevel(LogManager.ROOT_LOGGER_NAME, Level.ALL);
        return LogManager.getLogger(name);
    }

    public static Logger getLogger() {
        return getLogger(Utils.class);
    }

    public static ObjectMapper getObjectMapper() {
        DateFormat df = new SimpleDateFormat(ISO_8601_PATTERN);
        return new ObjectMapper().setDateFormat(df);
    }

    public static RestTemplate getRestTemplate() {
        RestTemplate rest = new RestTemplate();
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(getObjectMapper());
        rest.getMessageConverters().add(0, converter);
        return rest;

    }
}
