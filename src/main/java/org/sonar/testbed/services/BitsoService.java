package org.sonar.testbed.services;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import io.reactivex.subjects.PublishSubject;

import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sonar.testbed.types.Trade;
import org.sonar.testbed.types.TradeRow;
import org.sonar.testbed.types.TradeUpdate;
import org.sonar.testbed.Utils;
import org.sonar.testbed.types.OrderBook;
import org.sonar.testbed.types.OrderBookUpdate;
import org.sonar.testbed.types.OrderRow;
import org.sonar.testbed.types.OrderUpdateMessage;

public class BitsoService {
    private static final String TYPE_KEY = "type";
    private static final String BOOK_KEY = "book";
    private static final String ORDER_UPDATE_TYPE = "diff-orders";
    private static final String DEFAULT_BOOK = "btc_mxn";
    private static final String API_SERVER_BASE_URL = "https://api.bitso.com/v3";
//    private static final String API_SERVER_BASE_URL = "https://api-dev.bitso.com/v3";

    private static final Logger log = LogManager.getLogger(BitsoService.class);

    private static boolean isMock = false;

    // To use mock request response when Bitso kicks you off the server(rate limit or whatever)
    public static void setMockery(boolean shouldMock) {
        isMock = shouldMock;
    }

    public static boolean isMockery() {
        return isMock;
    }

    public static HttpHeaders getHeaders() {
        final HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko/20100101 Firefox/57.0");
        return headers;
    }

    public static void getBooks() {
        final String uri = API_SERVER_BASE_URL + "/available_books";

        final RestTemplate restTemplate = new RestTemplate();
        final HttpHeaders headers = getHeaders();
        final HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        final ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);

        log.debug(result);
    }

    // TODO: Cleanup code and make it simpler with uri map.
    // 1. Objects for each type - order, trade,
    public static OrderBook getOrderBook() {
        if (isMock) {
            return new OrderBook(new Date(), (List)new ArrayList<OrderRow>(), (List)new ArrayList<OrderRow>(), 0);
        }
        final String uri = API_SERVER_BASE_URL + "/order_book?book=BTC_MXN&aggregate=false";
        final HttpHeaders headers = getHeaders();
        final HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        OrderBookUpdate response;
        try {
            final ResponseEntity<OrderBookUpdate> result = Utils.getRestTemplate().exchange(uri, HttpMethod.GET, entity,
                    OrderBookUpdate.class);
            response = result.getBody();
        } catch (HttpServerErrorException e) {
            log.error(e.getResponseBodyAsString());
            log.error(e.getStatusText());
            try {
                response = Utils.getObjectMapper().readerFor(OrderBookUpdate.class).readValue(e.getResponseBodyAsString());
            } catch (IOException ioe) {
                log.error(ioe);
                throw e;
            }

        } catch (HttpClientErrorException e) {
            try {
                log.error(e.getResponseBodyAsString());
                response = Utils.getObjectMapper().readerFor(OrderBookUpdate.class).readValue(e.getResponseBodyAsString());
                log.error(response);
            } catch (IOException ioe) {
                log.error(ioe);
                throw e;
            }
        }
        return response.isSuccess() ? response.getOrderBook() : null;
    }

    public static List<Trade> getTrades() {
        final RestTemplate restTemplate = new RestTemplate();

        if (isMock) {
            String json ="{\"success\":true,\"payload\":[{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:56:09+0000\",\"amount\":\"0.01368209\",\"maker_side\":\"sell\",\"price\":\"219264.70\",\"tid\":2243249},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:56:05+0000\",\"amount\":\"0.00250000\",\"maker_side\":\"buy\",\"price\":\"217580.00\",\"tid\":2243248},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:56:01+0000\",\"amount\":\"0.00020000\",\"maker_side\":\"buy\",\"price\":\"217580.00\",\"tid\":2243247},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:55:58+0000\",\"amount\":\"0.00198000\",\"maker_side\":\"buy\",\"price\":\"217580.00\",\"tid\":2243246},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:55:43+0000\",\"amount\":\"0.00456037\",\"maker_side\":\"sell\",\"price\":\"219282.85\",\"tid\":2243243},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:55:37+0000\",\"amount\":\"0.00020000\",\"maker_side\":\"buy\",\"price\":\"217530.00\",\"tid\":2243241},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:55:34+0000\",\"amount\":\"0.00433231\",\"maker_side\":\"buy\",\"price\":\"217530.00\",\"tid\":2243240},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:55:32+0000\",\"amount\":\"0.00433231\",\"maker_side\":\"buy\",\"price\":\"219282.86\",\"tid\":2243239},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:55:26+0000\",\"amount\":\"0.02827580\",\"maker_side\":\"sell\",\"price\":\"219268.77\",\"tid\":2243238},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:55:18+0000\",\"amount\":\"0.01289828\",\"maker_side\":\"sell\",\"price\":\"219278.84\",\"tid\":2243237},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:55:14+0000\",\"amount\":\"0.00020000\",\"maker_side\":\"buy\",\"price\":\"217530.00\",\"tid\":2243236},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:55:06+0000\",\"amount\":\"0.00456011\",\"maker_side\":\"sell\",\"price\":\"219292.94\",\"tid\":2243231},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:55:03+0000\",\"amount\":\"0.01368058\",\"maker_side\":\"sell\",\"price\":\"219288.92\",\"tid\":2243229},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:54:46+0000\",\"amount\":\"0.00020000\",\"maker_side\":\"buy\",\"price\":\"217529.00\",\"tid\":2243225},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:54:42+0000\",\"amount\":\"0.02422486\",\"maker_side\":\"sell\",\"price\":\"219292.95\",\"tid\":2243224},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:54:42+0000\",\"amount\":\"0.00400000\",\"maker_side\":\"sell\",\"price\":\"219000.00\",\"tid\":2243223},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:54:29+0000\",\"amount\":\"0.02202010\",\"maker_side\":\"sell\",\"price\":\"219268.79\",\"tid\":2243221},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:54:21+0000\",\"amount\":\"0.00020000\",\"maker_side\":\"buy\",\"price\":\"217523.06\",\"tid\":2243220},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:54:18+0000\",\"amount\":\"0.00500000\",\"maker_side\":\"buy\",\"price\":\"217520.00\",\"tid\":2243219},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:53:58+0000\",\"amount\":\"0.04301501\",\"maker_side\":\"sell\",\"price\":\"219283.88\",\"tid\":2243217},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:53:58+0000\",\"amount\":\"0.00258897\",\"maker_side\":\"sell\",\"price\":\"219200.00\",\"tid\":2243216},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:53:46+0000\",\"amount\":\"0.09120000\",\"maker_side\":\"buy\",\"price\":\"217517.01\",\"tid\":2243214},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:53:40+0000\",\"amount\":\"0.01290384\",\"maker_side\":\"buy\",\"price\":\"217523.06\",\"tid\":2243213},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:53:39+0000\",\"amount\":\"0.00020000\",\"maker_side\":\"buy\",\"price\":\"217517.01\",\"tid\":2243212},{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:53:37+0000\",\"amount\":\"0.00333407\",\"maker_side\":\"buy\",\"price\":\"217523.06\",\"tid\":2243211}]}";
            try {
                TradeUpdate update = Utils.getObjectMapper().readerFor(TradeUpdate.class).readValue(json);
                return (List)update.getTrades();
            } catch (IOException e) {
                log.error(e);
            }
            return null;
        }

        final String uri = API_SERVER_BASE_URL + "/trades?book=btc_mxn";

        final HttpHeaders headers = getHeaders();
        final HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        TradeUpdate response;
        try {
            final ResponseEntity<TradeUpdate> result = restTemplate.exchange(uri, HttpMethod.GET, entity,
                    TradeUpdate.class);
            log.trace(result);
            response = result.getBody();
        } catch (HttpServerErrorException e) {
            log.error(e.getResponseBodyAsString());
            log.error(e.getStatusText());
            try {
                log.info("Using empty trades as trades call was rejected");
                response = new TradeUpdate(true, new ArrayList<TradeRow>(), null);
                if (false) response = Utils.getObjectMapper().readerFor(TradeUpdate.class).readValue(e.getResponseBodyAsString());
            } catch (IOException ioe) {
                log.error(ioe);
                throw e;
            }

        } catch (HttpClientErrorException e) {
            try {
                log.error(e.getResponseBodyAsString());
                response = Utils.getObjectMapper().readerFor(TradeUpdate.class).readValue(e.getResponseBodyAsString());
                log.error(response);
            } catch (IOException ioe) {
                log.error(ioe);
                throw e;

            }
        }
        return response.isSuccess() ? (List)response.getTrades() : null;
    }

    public static PublishSubject<OrderUpdateMessage> listenToOrderUpdates() throws URISyntaxException {
        WebSocketClient simpleWebSocketClient = new StandardWebSocketClient();
        URI url = new URI("wss://ws.bitso.com");

        PublishSubject<OrderUpdateMessage> orderSubject = PublishSubject.create();

        WebSocketHandler socketHandler = new WebSocketHandler() {

            @Override
            public boolean supportsPartialMessages() {
                log.debug("support partial msg");
                return false;
            }

            @Override
            public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
                log.error(exception);
                orderSubject.onError(exception);
            }

            @Override
            public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
                String msgText = (String) message.getPayload();
                log.trace(msgText);
                try {
                    // TODO: More efficient parser using ResponseEntity sort of handler
                    JsonNode orderUpdateNode = Utils.getObjectMapper().readTree(msgText);
                    JsonNode typeNode = orderUpdateNode.path(TYPE_KEY);
                    JsonNode bookNode = orderUpdateNode.path(BOOK_KEY);
                    if (!typeNode.isMissingNode() && typeNode.asText().equalsIgnoreCase(ORDER_UPDATE_TYPE)
                            && bookNode.asText().equalsIgnoreCase(DEFAULT_BOOK)) {

                        OrderUpdateMessage orderUpdate = Utils.getObjectMapper()
                                // ignore fields like type and book
                                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
                                .readerFor(OrderUpdateMessage.class).readValue(orderUpdateNode);
                        orderSubject.onNext(orderUpdate);
                    }
                    // TODO: check keep alive and make sure to throw error on no messages
                } catch (Exception e) {
                    orderSubject.onError(e);
                }
            }

            @Override
            public void afterConnectionEstablished(WebSocketSession session) throws Exception {
                log.debug("ws connection established: sending subscription message");
                String msg = "{ \"action\": \"subscribe\", \"book\": \"btc_mxn\", \"type\": \"diff-orders\" }";

                session.sendMessage(new TextMessage(msg.getBytes()));
                log.debug("ws connection established: sent subscription message");
            }

            @Override
            public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {
                log.debug("ws connection closed" + closeStatus);
                orderSubject.onComplete();
            }
        };

        WebSocketHttpHeaders headers = new WebSocketHttpHeaders(getHeaders());
        simpleWebSocketClient.doHandshake(socketHandler, headers, url);

        return orderSubject;
    }
}
