package org.sonar.testbed.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.sonar.testbed.Utils;
import org.sonar.testbed.strategies.Strategy;
import org.sonar.testbed.types.Order;
import org.sonar.testbed.types.Position;
import org.sonar.testbed.types.Trade;
import org.sonar.testbed.types.TradeRow;

public class TradeSimulationService implements TradingService {

    private static final Logger log = Utils.getLogger(TradeSimulationService.class);
    // Use a map ideally
    private List<TradeRow> trades = new ArrayList<TradeRow>();
    private Position position = new Position(0, 0);
    private double currentPrice = 0;
    private List<Strategy> listeners = new ArrayList<>();

    public TradeSimulationService() {
    }

    @Override
    public void sendOrder(Order o) {
    }

    @Override
    public void registerTrade(Trade t) {
        position.updateWithTrade(t);
        currentPrice = t.getPrice();
        TradeRow trade = new TradeRow(t.getId(), t.getCreationTime(), t.getSide().toString(), t.getAmount(), t.getPrice());
        trades.add(trade);
        listeners.forEach(l -> l.OnTradeUpdate(trade));
    }

    public void addListener(Strategy listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public void removeListener(Strategy listener) {
        listeners.remove(listener);
    }

    public List<TradeRow> getTrades() {
        return trades;
    }

    public Position getPosition() {
        return position;
    }

    public double getNetPosition() {
        return position.getMxn() + position.getBtc() * currentPrice;
    }
}
