package org.sonar.testbed.services;

import org.sonar.testbed.types.Order;
import org.sonar.testbed.types.Trade;

public interface TradingService {
    void sendOrder(Order o);
    void registerTrade(Trade t);
}
