package org.sonar.testbed;

import java.net.URISyntaxException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.SynchronousQueue;

import org.apache.logging.log4j.Logger;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.sonar.testbed.services.BitsoService;
import org.sonar.testbed.types.OrderBook;
import org.sonar.testbed.types.OrderUpdateMessage;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;

// This class is meant to handle refresh of who data during transport errors and such
// Not implemented fully
public class OrderBookManager implements Observer<OrderUpdateMessage> {

    private static final Logger log = Utils.getLogger(OrderBookManager.class);

    private OrderBook orderBook;
    private Disposable subscription;
    private Queue<OrderUpdateMessage> messageQueue;
//    private IObservable<OrderUpdateMessage>

    public OrderBookManager() {
        refreshOrderBook();
    }

    public void refreshOrderBook() {
        try {
            synchronized (this) {
                subscription.dispose();
                Queue<OrderUpdateMessage> newMessageQueue = new ConcurrentLinkedQueue<OrderUpdateMessage>();
                BitsoService.listenToOrderUpdates().subscribe(this);


            }
        } catch (URISyntaxException e) {
            log.error(e);
            log.info("Retrying order book refresh");
            refreshOrderBook();
        }
    }

    @Override
    public void onSubscribe(Disposable d) {
        subscription.dispose();
        subscription = d;
    }

    @Override
    public void onNext(OrderUpdateMessage m) {
        synchronized(this) {
            messageQueue.add(m);
        }
    }

    @Override
    public void onError(Throwable e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onComplete() {
        // TODO Auto-generated method stub

    }


}
