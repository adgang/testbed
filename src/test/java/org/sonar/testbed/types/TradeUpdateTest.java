package org.sonar.testbed.types;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.sonar.testbed.TestUtils;
import org.sonar.testbed.Utils;

public class TradeUpdateTest {
    Logger log = TestUtils.getLogger(TradeUpdateTest.class);

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public final void testTradeUpdateDeSer() throws IOException {
        String json = "{" + "\"success\":true,\"payload\":["
                + "{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:56:09+0000\",\"amount\":\"0.01368209\",\"maker_side\":\"sell\",\"price\":\"219264.70\",\"tid\":2243249},"
                + "{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:56:05+0000\",\"amount\":\"0.00250000\",\"maker_side\":\"buy\",\"price\":\"217580.00\",\"tid\":2243248},"
                + "{\"book\":\"btc_mxn\",\"created_at\":\"2017-12-03T17:56:01+0000\",\"amount\":\"0.00020000\",\"maker_side\":\"buy\",\"price\":\"217580.00\",\"tid\":2243247}"
                + "]}";

        TradeUpdate tradeUpdate = null;
        try {
            tradeUpdate = Utils.getObjectMapper().readerFor(TradeUpdate.class).readValue(json);
            assertEquals(true, tradeUpdate.isSuccess());
            List<TradeRow> trades = tradeUpdate.getTrades();
            assertEquals(3, trades.size());
            assertEquals(TestUtils.getTime(2017, 11, 03, 17, 56, 9), trades.get(0).getCreationTime());
            assertEquals(Side.SELL, trades.get(0).getSide());
            assertEquals(Side.BUY, trades.get(1).getSide());

            assertEquals(2243248, trades.get(1).getId());
            assertEquals(217580.00, trades.get(1).getPrice(), 0.0);
            assertEquals(0.00020000, trades.get(2).getAmount(), 0.0);
        } catch (IOException e) {
            log.error(e);
            throw e;
        }

    }

}
