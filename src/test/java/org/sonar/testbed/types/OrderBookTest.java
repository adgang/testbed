/**
 *
 */
package org.sonar.testbed.types;

import static org.junit.Assert.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.sonar.testbed.TestUtils;
import org.sonar.testbed.Utils;

/**
 * @author adgang
 *
 */
public class OrderBookTest {
     Logger log = TestUtils.getLogger(OrderBookTest.class);

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    @Test
    public final void testOrderBookDeSer() throws IOException, ParseException {
        String json =
                "{\"updated_at\":\"2017-12-03T17:56:12+00:00\","
                + "\"bids\":[{\"book\":\"btc_mxn\",\"price\":\"217580.00\",\"amount\":\"0.08724022\",\"oid\":\"012uju\"},{\"book\":\"btc_mxn\",\"price\":\"217556.05\",\"amount\":\"0.12576798\",\"oid\":\"7xhXrw05yr0FQoZq\"}], "
                + "\"asks\":[{\"book\":\"btc_mxn\",\"price\":\"219272.78\",\"amount\":\"0.13241798\",\"oid\":\"112uju\"},{\"book\":\"btc_mxn\",\"price\":\"219299.00\",\"amount\":\"0.05869488\",\"oid\":\"22dQOIk63BEhdpdg\"}], "
                + "\"sequence\":\"44712258\"}";
        OrderBook orderBook = null;
        try {
            orderBook = Utils.getObjectMapper().readerFor(OrderBook.class).readValue(json);
            OrderMap bids, asks;
            bids = orderBook.getBids();
            asks = orderBook.getAsks();
            assertEquals(2, bids.size());
            assertEquals(2, asks.size());
            assertEquals(217580.00, bids.get("012uju").getPrice(), 0.00);
            assertEquals(0.13241798, asks.get("112uju").getAmount(), 0.00000000000);
            assertEquals(TestUtils.getTime(2017, 11, 03, 17, 56, 12), orderBook.getLastUpdated());
            assertEquals(44712258, orderBook.getSequence());
        } catch (IOException e) {
            log.error(e);
            throw e;
        }
    }

}
