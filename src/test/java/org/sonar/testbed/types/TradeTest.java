package org.sonar.testbed.types;

import static org.junit.Assert.*;

import java.io.IOException;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.sonar.testbed.TestUtils;
import org.sonar.testbed.Utils;

public class TradeTest {
    Logger log = TestUtils.getLogger(TradeTest.class);

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public final void testTradeDeSer() throws IOException {
        String json = "{" + "\"book\":\"btc_mxn\"," + "\"created_at\":\"2017-12-03T17:56:09+0000\","
                + "\"amount\":\"0.01368209\"," + "\"maker_side\":\"sell\"," + "\"price\":\"219264.70\","
                + "\"tid\":2243249" + "}";

        Trade trade = null;
        try {
            trade = Utils.getObjectMapper().readerFor(Trade.class).readValue(json);
            assertEquals("btc_mxn", trade.getBook());
            assertEquals(2243249, trade.getId());
            assertEquals(TestUtils.getTime(2017, 11, 03, 17, 56, 9), trade.getCreationTime());
            assertEquals(Side.SELL, trade.getSide());
            assertEquals(0.01368209, trade.getAmount(), 0.0);
            assertEquals(219264.70, trade.getPrice(), 0.0);
        } catch (IOException e) {
            log.error(e);
            throw e;
        }

    }

}
