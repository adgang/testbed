package org.sonar.testbed.types;

import static org.junit.Assert.*;

import java.util.Comparator;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.sonar.testbed.TestUtils;

public class BidTreeMapTest {

    private static final Logger log = TestUtils.getLogger(BidTreeMapTest.class);

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public final void testTreeMap() {
        BidTreeMap map = new BidTreeMap();
        Order o1 = new Order("a", 2.3, 1.2);
        Order o2 = new Order("b", 2.2, 1.5);
        Order o3 = new Order("c", 2.4, 1.3);


        map.put(o1.getOrderId(), o1);
        log.info(map);
        log.info(map.navigableKeySet());

        map.put(o2.getOrderId(), o2);
        log.info(map);
        log.info(map.navigableKeySet());

        map.put(o3.getOrderId(), o3);
        log.info(map);
        log.info(map.navigableKeySet());

        map.put(o2.getOrderId(), new Order(o2.getOrderId(), 2.5, 1.7));
        log.info(map);
        log.info(map.navigableKeySet());

        map.navigableKeySet().stream().forEach(k -> log.info(k));
        log.info(map.keySet());
        log.info(map.descendingKeySet());
        log.info(map.navigableKeySet());



    }
}
