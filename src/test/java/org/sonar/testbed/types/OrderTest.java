/**
 *
 */
package org.sonar.testbed.types;

import static org.junit.Assert.*;

import java.io.IOException;

import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.sonar.testbed.TestUtils;
import org.sonar.testbed.Utils;


/**
 * @author adgang
 *
 */
public class OrderTest {
     Logger log = TestUtils.getLogger(OrderTest.class);


    /**
     * @param log
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    @Test
    public final void testPlainOrder() throws IOException {
        String json =
                "{\"book\":\"btc_mxn\",\"price\":221524.07,\"amount\":0.00763903}";
        Order o = null;
        try {
            o = Utils.getObjectMapper().readerFor(Order.class).readValue(json);
            assertEquals("btc_mxn", o.getBook());
            assertEquals(0.00763903, o.getAmount(), 0.000000001);
            assertEquals(221524.07, o.getPrice(), 0.001);
        } catch (IOException e) {
            log.error(e);
            throw e;
        }
    }

}
