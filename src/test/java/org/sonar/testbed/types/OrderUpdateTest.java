package org.sonar.testbed.types;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Date;

import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.sonar.testbed.TestUtils;
import org.sonar.testbed.Utils;

public class OrderUpdateTest {
    Logger log = TestUtils.getLogger(OrderUpdateTest.class);

    @Before
    public void setUp() throws Exception {
    }


    @Test
    public final void testOrderUpdate() throws IOException {
        String json =
                "{\"o\":\"dhsoFqNaSgA8pxmC\","
                + "\"d\":1512323780815,"
                + "\"r\":\"250593.87\","
                + "\"t\":1,"
                + "\"a\":\"0.00663565\","
                + "\"v\":\"1662.85321347\","
                + "\"s\":\"open\"}";
        OrderUpdate orderUpdate = null;
        try {
            orderUpdate = Utils.getObjectMapper().readerFor(OrderUpdate.class).readValue(json);
            assertEquals("btc_mxn", orderUpdate.getBook());
            assertEquals(new Date(Long.parseLong("1512323780815")), orderUpdate.getTimestamp());
            assertEquals(250593.87, orderUpdate.getPrice(), 0.0);
            assertEquals(Side.SELL, orderUpdate.getSide());
            assertEquals(0.00663565, orderUpdate.getAmount(), 0.0);
            assertEquals(1662.85321347, orderUpdate.getValue(), 0.0);
            assertEquals("dhsoFqNaSgA8pxmC", orderUpdate.getOrderId());
            assertEquals("open", orderUpdate.getStatus());
        } catch (IOException e) {
            log.error(e);
            throw e;
        }
    }

}
