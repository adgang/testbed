package org.sonar.testbed.types;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.sonar.testbed.TestUtils;
import org.sonar.testbed.types.OrderMap.Change;
import org.sonar.testbed.types.OrderMap.ChangeListener;

public class OrderMapTest {

    private static final Logger log = TestUtils.getLogger(OrderMapTest.class);

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public final void testOrderMap() {

        OrderMap map = new OrderMap();
        map.add(new Order("a", 12.12, 100));
        log.info(oids(map.top(2)));

        map.add(new Order("b", 13.12, 100));
        log.info(oids(map.top(2)));

        map.add(new Order("c", 11.12, 100));
        log.info(oids(map.top(3)));

        assertEquals(oids(map.top(3)), makeList("b", "a", "c"));

        map.add(new Order("d", 10.12, 100));
        assertEquals(oids(map.top(6)), makeList("b", "a", "c", "d"));

        map.add(new Order("e", 12.52, 100));
        assertEquals(oids(map.top(6)), makeList("b", "e", "a", "c", "d"));

        map.add(new Order("f", 9.12, 100));
        assertEquals(oids(map.top(6)), makeList("b", "e", "a", "c", "d", "f"));

        assertEquals(oids(map.top(5)), makeList("b", "e", "a", "c", "d"));

        map.remove("a");
        assertEquals(oids(map.top(6)), makeList("b", "e", "c", "d", "f"));

        map.add(new Order("d", 1.12, 100));
        assertEquals(oids(map.top(6)), makeList("b", "e", "c", "f", "d"));

        map.add(new Order("d", 17.12, 100));
        assertEquals(oids(map.top(6)), makeList("d", "b", "e", "c", "f"));

        map.add(new Order("d", 12.00, 100));
        assertEquals(oids(map.top(6)), makeList("b", "e", "d", "c", "f"));
        log.debug(map.top(6));

    }

    @Test
    public final void testOrderMapListeners() {

        OrderMap map = new OrderMap();
        map.addListener(new ChangeListener() {
            @Override
            public void handleChange(Change c) {
                log.debug("changed index of " + c.getId() + " from " + c.getFrom() + " to " + c.getTo());
            }
        });
        map.add(new Order("a", 12.12, 100));
        log.debug(oids(map.top(2)));

        map.add(new Order("b", 13.12, 100));
        log.debug(oids(map.top(2)));

        map.add(new Order("c", 11.12, 100));
        log.debug(oids(map.top(3)));

        assertEquals(oids(map.top(3)), makeList("b", "a", "c"));

        map.add(new Order("d", 10.12, 100));
        assertEquals(oids(map.top(6)), makeList("b", "a", "c", "d"));
        log.debug(oids(map.top(6)));

        map.add(new Order("e", 12.52, 100));
        assertEquals(oids(map.top(6)), makeList("b", "e", "a", "c", "d"));
        log.debug(oids(map.top(6)));


        map.add(new Order("f", 9.12, 100));
        log.debug(oids(map.top(6)));
        assertEquals(oids(map.top(6)), makeList("b", "e", "a", "c", "d", "f"));

        assertEquals(oids(map.top(5)), makeList("b", "e", "a", "c", "d"));

        map.remove("a");
        log.debug(oids(map.top(6)));

        assertEquals(oids(map.top(6)), makeList("b", "e", "c", "d", "f"));

        map.add(new Order("d", 1.12, 100));
        log.debug(oids(map.top(6)));
        assertEquals(oids(map.top(6)), makeList("b", "e", "c", "f", "d"));

        map.add(new Order("d", 17.12, 100));
        log.debug(oids(map.top(6)));
        assertEquals(oids(map.top(6)), makeList("d", "b", "e", "c", "f"));

        map.add(new Order("d", 12.00, 100));
        log.debug(oids(map.top(6)));
        assertEquals(oids(map.top(6)), makeList("b", "e", "d", "c", "f"));
        log.debug(map.top(6));

    }

    @Test
    public final void testBidMap() {

        OrderMap map = new OrderMap();
        map.add(new Order("a", 12.12, 100));
        log.info(oids(map.top(2)));

        map.add(new Order("b", 13.12, 100));
        log.info(oids(map.top(2)));

        map.add(new Order("c", 11.12, 100));
        log.info(oids(map.top(3)));

        assertEquals(oids(map.top(3)), makeList("b", "a", "c"));

        map.add(new Order("d", 10.12, 100));
        assertEquals(oids(map.top(6)), makeList("b", "a", "c", "d"));

        map.add(new Order("e", 12.52, 100));
        assertEquals(oids(map.top(6)), makeList("b", "e", "a", "c", "d"));

        map.add(new Order("f", 9.12, 100));
        assertEquals(oids(map.top(6)), makeList("b", "e", "a", "c", "d", "f"));

        assertEquals(oids(map.top(5)), makeList("b", "e", "a", "c", "d"));

    }

    @Test
    public final void testAskMap() {
        OrderMap map = new OrderMap(new Comparator<Order>() {

            @Override
            public int compare(Order o1, Order o2) {
                return Double.compare(o1.getPrice(), o2.getPrice());
            }
        });
        map.add(new Order("a", 12.12, 100));
        log.info(oids(map.top(2)));

        map.add(new Order("b", 13.12, 100));
        log.info(oids(map.top(2)));

        map.add(new Order("c", 11.12, 100));
        log.info(oids(map.top(3)));
        assertEquals(oids(map.top(3)), makeList("c", "a", "b"));

        map.add(new Order("d", 10.12, 100));
        assertEquals(oids(map.top(6)), makeList("d", "c", "a", "b"));

        map.add(new Order("e", 12.52, 100));
        assertEquals(oids(map.top(6)), makeList("d", "c", "a", "e", "b"));

        map.add(new Order("f", 9.12, 100));
        assertEquals(oids(map.top(6)), makeList("f", "d", "c", "a", "e", "b"));

        assertEquals(oids(map.top(5)), makeList("f", "d", "c", "a", "e"));

    }

    public List<String> makeList(String ... str) {
        List<String> list = new ArrayList<String>();
        for (String s : str) {
            list.add(s);
        }
        return list;

    }

    public List<String> oids(List<? extends Order> orders) {
        return orders.stream().map(o -> o.getOrderId()).collect(Collectors.toList());
    }
}
