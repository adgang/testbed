package org.sonar.testbed.types;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.sonar.testbed.TestUtils;
import org.sonar.testbed.Utils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class UtilsTest {
    Logger log = TestUtils.getLogger(UtilsTest.class);

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public final void testDateTimeFormat() throws JsonMappingException, JsonParseException, IOException {
        String dateString = "\"2017-12-03T17:56:12+00:00\"";
        try {
            Date actualDate = Utils.getObjectMapper().readerFor(Date.class).readValue(dateString);
            Calendar c = Calendar.getInstance();
            c.setTimeZone(TimeZone.getTimeZone("GMT"));
            c.setTimeInMillis(0);
            c.set(2017, 11, 03, 17, 56, 12);
            Date expectedDate = c.getTime();
            log.debug("expected date:" + expectedDate.toString() + expectedDate.getTime());
            log.debug("actual date:" +  actualDate.toString() + actualDate.getTime());
            assertEquals(expectedDate.compareTo(actualDate), 0);
        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
            throw e;
        }
    }

}
