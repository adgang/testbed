package org.sonar.testbed;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

/**
 * Utility functions for test cases
 * @author adgang
 *
 */
public class TestUtils {
    public static Logger getLogger(Class<?> name) {
        Logger logger = LogManager.getLogger(name);
        Configurator.setLevel(LogManager.ROOT_LOGGER_NAME, Level.ALL);
        return logger;
    }

    public static Logger getLogger() {
        return getLogger(TestUtils.class);
    }


    public static Date getTime(int year, int month, int day,
            int hours, int minutes, int seconds) {
        Calendar c = Calendar.getInstance();
        c.setTimeZone(TimeZone.getTimeZone("GMT"));
        c.setTimeInMillis(0);
        c.set(year, month, day, hours, minutes, seconds);
        return c.getTime();
    }
}
