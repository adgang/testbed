package org.sonar.testbed.strategies;

import static org.junit.Assert.*;

import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.sonar.testbed.TestUtils;
import org.sonar.testbed.services.TradeSimulationService;
import org.sonar.testbed.types.Trade;

public class MarketMakerStrategyTest {

    private static final Logger log = TestUtils.getLogger(UpUpUpStrategyTest.class);

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public final void testMarketMakerStrategy() {
        TradeSimulationService service = new TradeSimulationService();
        Strategy strat =  new MarketMakerStrategy(service);

        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.4));
        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.5));
        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.6));
        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.7));

        log.info(service.getPosition().getBtc());
        log.info(service.getNetPosition());

        assertEquals(1.0, service.getPosition().getBtc(), 0.0);
        assertEquals(service.getPosition().getMxn(), -0.7, 0.0000000001);
        assertEquals(service.getNetPosition(), 0, 0.0);
        log.info(service.getPosition().getBtc());
        log.info(service.getNetPosition());

    }

    @Test
    public final void testBuyNSellStrategy() {
        TradeSimulationService service = new TradeSimulationService();
        MarketMakerStrategy strat =  new MarketMakerStrategy(service);

        strat.setDowntickCountTrigger(2);

        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.4));
        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.5));
        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.6));
        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.7));
        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.6));
        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.5));
        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.4));
        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.7));


        assertEquals(0, service.getPosition().getBtc(), 0.0);
        assertEquals(-0.2, service.getPosition().getMxn(), 0.0000000001);
        assertEquals(-0.2, service.getNetPosition(), 0.000001);
        log.info(service.getPosition().getBtc());
        log.info(service.getNetPosition());

    }

    @Test
    public final void testBuyNSellNSell() {
        TradeSimulationService service = new TradeSimulationService();
        MarketMakerStrategy strat =  new MarketMakerStrategy(service);

        strat.setDowntickCountTrigger(2);

        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.4));
        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.5));
        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.6));
        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.7));
        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.6));
        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.5));
        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.4));
        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.3));
        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.2));


        assertEquals(0, service.getPosition().getBtc(), 0.0);
        assertEquals(-0.2, service.getPosition().getMxn(), 0.0000000001);
        assertEquals(-0.2, service.getNetPosition(), 0.000001);
        log.info(service.getPosition().getBtc());
        log.info(service.getNetPosition());

    }
}
