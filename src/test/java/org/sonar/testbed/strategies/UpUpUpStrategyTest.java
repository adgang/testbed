package org.sonar.testbed.strategies;

import static org.junit.Assert.*;

import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.sonar.testbed.TestUtils;
import org.sonar.testbed.services.TradeSimulationService;
import org.sonar.testbed.types.Trade;

public class UpUpUpStrategyTest {

    private static final Logger log = TestUtils.getLogger(UpUpUpStrategyTest.class);

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public final void testUpUpStrategy() {
        TradeSimulationService service = new TradeSimulationService();
        Strategy strat =  new UpUpUpStrategy(service);

        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.4));
        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.5));
        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.6));
        strat.OnTradeUpdate(new Trade(1, null, "SELL", 12, 0.7));

        assertEquals(service.getPosition().getBtc(), 12, 0.0);
        assertEquals(service.getPosition().getMxn(), -8.4, 0.0000000001);
        assertEquals(service.getNetPosition(), 0, 0.0);
        log.info(service.getPosition().getBtc());
        log.info(service.getNetPosition());

    }
}
