/**
 *
 */
package org.sonar.testbed;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author adgang
 *
 * Write any test cases for JSON serialization and experiments here
 */
public class JSONSerialization {

    /**
     * @param args
     */
    public static void main(String[] args) {
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            ObjectMapper myObjectMapper =  new ObjectMapper();
            Item itemWithOwner = myObjectMapper.setDateFormat(df).readerFor(Item.class).readValue(JSONSerialization.JSON_ITEM);
            System.out.println(itemWithOwner.toString());
            System.out.println(itemWithOwner.edate);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static class User {
        public int id;
        // @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd")
        public String name;

        public String toString() {
                return "{" + id + ":" + name + "}";
        }
    }
    public static class Item {
        public int id;
        public String itemName;
        public Date edate;

        public User owner;
        public String toString() {
                return "{" + id + "," + itemName + "," + owner + "}";
        }

    }

    public static final String JSON_ITEM =
            "{" +
            "    \"id\": \"1\"," +
            "	 \"edate\": \"2017-12-07\"," +
            "    \"itemName\": \"theItem\"," +
            "    \"owner\": {" +
            "        \"id\": 2," +
            "        \"name\": \"theUser\"" +
            "    }" +
            "}"
            ;




}
