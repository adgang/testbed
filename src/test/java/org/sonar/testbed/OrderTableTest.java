package org.sonar.testbed;

import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.junit.Before;
import org.junit.Test;
import org.sonar.testbed.services.BitsoService;
import org.sonar.testbed.types.Order;
import org.sonar.testbed.types.OrderBook;
import org.sonar.testbed.types.OrderBookHandler;
import org.sonar.testbed.types.OrderUpdate;
import org.sonar.testbed.types.OrderUpdateMessage;
import org.sonar.testbed.types.Side;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class OrderTableTest {

    private static final Logger log = TestUtils.getLogger(OrderTableTest.class);

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public final void testOrderTable() throws URISyntaxException, InterruptedException {
        OrderTable table = new OrderTable(new OrderBookHandler() {

            @Override
            public void handleError(Throwable t) {
                log.error("error:" + t);
            }

            @Override
            public void onOrderBookReady(OrderBook book) {

            }
        });

        Configurator.setLevel(LogManager.ROOT_LOGGER_NAME, Level.DEBUG);
        Observable<OrderUpdateMessage> obs = BitsoService.listenToOrderUpdates();
        table.fetchOrders(obs);

        Observer<OrderUpdateMessage> observer = new Observer<OrderUpdateMessage>() {

            @Override
            public void onSubscribe(Disposable d) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onNext(OrderUpdateMessage t) {
                List<String> oidList = t.getOrderUpdates().stream().filter(p -> p.getSide() == Side.SELL)
                        .map(p -> p.getOrderId()).collect(Collectors.toList());
                if (oidList.size() > 0) {
                    List<? extends Order> asks = table.getAsks(10);
                    if (asks.stream().anyMatch(a -> oidList.contains(a.getOrderId()))) {
                        for (OrderUpdate u : t.getOrderUpdates()) {
                            if (u.getOrderId() == oidList.get(0)) {
                                log.debug(u);
                                log.debug("Asks:" + asks);
                            }
                        }
                        ;
                    }
                }
                ;
            }

            @Override
            public void onError(Throwable e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onComplete() {
                // TODO Auto-generated method stub

            }

        };
        obs.subscribe(observer);
        table.fetchOrders(obs);
        obs.subscribe(observer);

        Thread.sleep(2500);
        table.dispose();
    }
}
