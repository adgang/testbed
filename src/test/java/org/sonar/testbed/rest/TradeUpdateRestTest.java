package org.sonar.testbed.rest;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.sonar.testbed.TestUtils;
import org.sonar.testbed.services.BitsoService;
import org.sonar.testbed.types.Trade;

public class TradeUpdateRestTest {

    private static final Logger log = TestUtils.getLogger(TradeUpdateRestTest.class);

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public final void testTradeUpdateResponse() {
        List<Trade> trades = BitsoService.getTrades();
        log.debug(trades);

        // REVIEW: will fail when number of trades < 24 in response
        assertEquals(25, trades.size());
        assert (trades.get(0).getAmount() > 0.0);
        assert (trades.get(0).getPrice() > 10000.0);
        // Trade should be in the past.
        // REVIEW: will fail when number of trades < 24 in response
        assert (trades.get(24).getCreationTime().compareTo(new Date()) == -1);

    }
}
