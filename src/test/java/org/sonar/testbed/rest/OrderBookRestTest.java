/**
 *
 */
package org.sonar.testbed.rest;

import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.sonar.testbed.TestUtils;
import org.sonar.testbed.services.BitsoService;
import org.sonar.testbed.types.OrderBook;

/**
 * @author adgang
 *
 */
// TODO: Group these in integration test group and don't run unless explicitly asked
// Otherwise these will break under no network conditions
public class OrderBookRestTest {
    private static final Logger log = TestUtils.getLogger(OrderBookRestTest.class);

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    @Test
    public final void testRestOrderBook() {
        OrderBook book = BitsoService.getOrderBook();
        log.debug(book.getLastUpdated());
        assert(book.getAsks().size() > 0);
    }

}
